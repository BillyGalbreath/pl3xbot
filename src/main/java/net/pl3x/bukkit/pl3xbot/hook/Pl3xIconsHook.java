package net.pl3x.bukkit.pl3xbot.hook;

import net.pl3x.bukkit.pl3xicons.api.IconManager;

public class Pl3xIconsHook {
    /**
     * Get translated string from Pl3xIcons plugin
     * <p>
     * Changes {icons} to unicode icons
     *
     * @param string String to translate
     * @return Translated string
     */
    public String translate(String string) {
        return IconManager.getManager().translate(string);
    }

    /**
     * Get untranslated string from Pl3xIcons plugin
     * <p>
     * Changes unicode icons to {icons}
     *
     * @param string String to untranslate
     * @return Untranslated string
     */
    public String untranslate(String string) {
        return IconManager.getManager().untranslate(string);
    }
}
