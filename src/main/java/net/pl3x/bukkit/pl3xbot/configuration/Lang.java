package net.pl3x.bukkit.pl3xbot.configuration;

import net.pl3x.bukkit.pl3xbot.Pl3xBot;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;

public class Lang {
    public static String COMMAND_NO_PERMISSION;

    public static String CONNECT_SUCCESS;
    public static String CONNECT_FAILED;
    public static String DISCONNECT_SUCCESS;
    public static String DISCONNECT_FAILED;

    public static String MINECRAFT_FORMAT;
    public static String DISCORD_FORMAT;

    public static String UNKNOWN_COMMAND;
    public static String COMMAND_TPS_RESULT;
    public static String COMMAND_LIST_RESULT;

    public static String VERSION;
    public static String RELOAD;

    public static void reload() {
        Pl3xBot plugin = Pl3xBot.getPlugin();
        String langFile = Config.LANGUAGE_FILE;
        File configFile = new File(plugin.getDataFolder(), langFile);
        if (!configFile.exists()) {
            plugin.saveResource(Config.LANGUAGE_FILE, false);
        }
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        COMMAND_NO_PERMISSION = config.getString("command-no-permission", "&4You do not have permission for this command!");

        CONNECT_SUCCESS = config.getString("connect-success", "&dConnect succeeded.");
        CONNECT_FAILED = config.getString("connect-failed", "&4Connect failed! See console for more details.");
        DISCONNECT_SUCCESS = config.getString("disconnect-success", "&dDisconnect succeeded.");
        DISCONNECT_FAILED = config.getString("disconnect-failed", "&4Disconnect failed! See console for more details.");

        MINECRAFT_FORMAT = config.getString("minecraft-format", "<{sender}> {message}");
        DISCORD_FORMAT = config.getString("discord-format", "**{sender}**: {message}");

        UNKNOWN_COMMAND = config.getString("unknown-command", "&4Unknown command!");
        COMMAND_TPS_RESULT = config.getString("command-tps-result", "Current TPS: {tps}");
        COMMAND_LIST_RESULT = config.getString("command-list-result", "There are {count}/{max} players online:\n{userlist}");

        VERSION = config.getString("version", "&d{plugin} v{version}.");
        RELOAD = config.getString("reload", "&d{plugin} v{version} reloaded.");
    }

    public static void send(CommandSender recipient, String message) {
        if (message == null) {
            return; // do not send blank messages
        }
        message = ChatColor.translateAlternateColorCodes('&', message);
        if (ChatColor.stripColor(message).isEmpty()) {
            return; // do not send blank messages
        }

        for (String part : message.split("\n")) {
            recipient.sendMessage(part);
        }
    }

    public static void broadcast(String message) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            send(player, message);
        }
        send(Bukkit.getConsoleSender(), message);
    }
}
