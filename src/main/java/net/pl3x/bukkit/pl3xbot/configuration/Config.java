package net.pl3x.bukkit.pl3xbot.configuration;

import net.pl3x.bukkit.pl3xbot.Pl3xBot;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static String TOKEN = "";
    public static String CHANNEL = "general";
    public static int MAX_RECONNECT_ATTEMPTS = 5;
    public static int PING_TIMEOUT = -1;
    public static String AVATAR = "";
    public static String STATUS = "Minecraft {version}";
    public static String COMMAND_TRIGGER = "!";
    public static String PREFIX = "&7[&3Discord&7] &r";


    public static void reload() {
        Pl3xBot plugin = Pl3xBot.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        TOKEN = config.getString("client.token", "");
        CHANNEL = config.getString("client.channel", "general");
        MAX_RECONNECT_ATTEMPTS = config.getInt("client.max-reconnect-attempts", 5);
        PING_TIMEOUT = config.getInt("client.ping-timeout", -1);
        AVATAR = config.getString("client.avatar", "");
        STATUS = config.getString("client.status", "Minecraft {version}");
        COMMAND_TRIGGER = config.getString("client.command-trigger", "!");
        PREFIX = config.getString("client.prefix", "&7[&3Discord&7] &r");
    }
}
