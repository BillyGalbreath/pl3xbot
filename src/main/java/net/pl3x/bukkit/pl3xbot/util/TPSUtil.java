package net.pl3x.bukkit.pl3xbot.util;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;

public class TPSUtil extends BukkitRunnable {
    private long lastTick;
    private final Deque<Long> tickIntervals;
    private final int resolution = 1200;

    public TPSUtil(Plugin plugin) {
        lastTick = System.currentTimeMillis();
        tickIntervals = new ArrayDeque<>(Collections.nCopies(resolution, 50L));
        this.runTaskTimer(plugin, 1, 1);
    }

    @Override
    public void run() {
        long curr = System.currentTimeMillis();
        long delta = curr - lastTick;
        lastTick = curr;
        tickIntervals.removeFirst();
        tickIntervals.addLast(delta);
    }

    public double getTPS() {
        int base = 0;
        for (long delta : tickIntervals) {
            base += delta;
        }
        double tps = 1000D / ((double) base / resolution);
        return tps > 20D ? 20D : tps;
    }
}
