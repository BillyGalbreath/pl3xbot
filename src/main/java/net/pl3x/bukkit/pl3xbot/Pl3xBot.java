package net.pl3x.bukkit.pl3xbot;

import com.vdurmont.emoji.EmojiParser;
import net.pl3x.bukkit.pl3xbot.command.CmdPl3xBot;
import net.pl3x.bukkit.pl3xbot.configuration.Config;
import net.pl3x.bukkit.pl3xbot.configuration.Lang;
import net.pl3x.bukkit.pl3xbot.hook.Pl3xIconsHook;
import net.pl3x.bukkit.pl3xbot.listener.BukkitListener;
import net.pl3x.bukkit.pl3xbot.listener.CommandListener;
import net.pl3x.bukkit.pl3xbot.listener.DiscordListener;
import net.pl3x.bukkit.pl3xbot.listener.MessageListener;
import net.pl3x.bukkit.pl3xbot.util.TPSUtil;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventDispatcher;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

public class Pl3xBot extends JavaPlugin {
    private Pl3xIconsHook pl3xIconsHook;
    private TPSUtil tpsUtil;
    private IDiscordClient client;
    private IChannel channel;

    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        if (!getServer().getPluginManager().isPluginEnabled("Discord4JWrapper")) {
            Logger.error("Dependency plugin Discord4JWrapper not found!");
            Logger.error("Disabling " + getName());
            return;
        }

        if (getServer().getPluginManager().isPluginEnabled("Pl3xIcons")) {
            Logger.info("Pl3xIcons found. Initiating hook.");
            pl3xIconsHook = new Pl3xIconsHook();
        }

        getServer().getPluginManager().registerEvents(new BukkitListener(this), this);
        getServer().getPluginManager().registerEvents(new CommandListener(this), this);
        getServer().getPluginManager().registerEvents(new MessageListener(this), this);

        getCommand("pl3xbot").setExecutor(new CmdPl3xBot(this));

        tpsUtil = new TPSUtil(this);

        getServer().getScheduler().runTaskLater(this, () -> {
            try {
                connect();
                Logger.info("Bot connected successfully");
            } catch (DiscordException | IllegalStateException e) {
                Logger.error("Bot connection failed:");
                e.printStackTrace();
            }
        }, 20);

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        try {
            disconnect();
            Logger.info("Bot disconnected");
        } catch (DiscordException e) {
            Logger.error("Bot disconnect failed:");
            e.printStackTrace();
        }

        Logger.info(getName() + " disabled.");
    }

    /**
     * Get plugin/bot instance
     *
     * @return Pl3xBot instance
     */
    public static Pl3xBot getPlugin() {
        return Pl3xBot.getPlugin(Pl3xBot.class);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4" + getName() + " is disabled. See console log for more information."));
        return true;
    }

    /**
     * Get the TPS from the last 5 seconds
     *
     * @return TPS
     */
    public double getTPS() {
        return tpsUtil.getTPS();
    }

    /**
     * Get Pl3xIcons hook
     *
     * @return Pl3xIconsHook, or null
     */
    public Pl3xIconsHook getPl3xIconsHook() {
        return pl3xIconsHook;
    }

    /**
     * Connect the bot to discord servers
     *
     * @throws DiscordException      Thrown if the instance isn't built correctly or there is an error logging in
     * @throws IllegalStateException Thrown if client is already set
     */
    public void connect() throws DiscordException {
        if (client != null) {
            throw new IllegalStateException("Client already set");
        }

        ClientBuilder clientBuilder = new ClientBuilder();
        clientBuilder.setMaxReconnectAttempts(Config.MAX_RECONNECT_ATTEMPTS);
        clientBuilder.withPingTimeout(Config.PING_TIMEOUT);
        clientBuilder.withToken(Config.TOKEN);

        client = clientBuilder.login();

        EventDispatcher dispatcher = client.getDispatcher();
        dispatcher.registerListener(new DiscordListener(this));
    }

    /**
     * Disconnect the bot from discord servers
     *
     * @throws DiscordException      Thrown if there was an error logging out
     * @throws IllegalStateException Thrown if no client is set
     */
    public void disconnect() throws DiscordException {
        if (client == null) {
            throw new IllegalStateException("No client is set");
        }
        client.logout();
        client = null;
    }

    /**
     * Get the discord4j client
     *
     * @return Discord4J client, or null
     */
    public IDiscordClient getClient() {
        return client;
    }

    /**
     * Get the bot name
     * <p>
     * If unable to determine bot name "Bot" is returned.
     *
     * @param client Bot client
     * @return Name of bot
     */
    public String getName(IDiscordClient client) {
        try {
            return client.getApplicationName();
        } catch (DiscordException ignore) {
            return "Bot";
        }
    }

    /**
     * Get the channel the bot is listening on
     */
    public IChannel getChannel() {
        return channel;
    }

    /**
     * Set the channel the bot is listening on
     *
     * @param channel The channel
     */
    public void setChannel(IChannel channel) {
        this.channel = channel;
    }

    /**
     * Send chat message to minecraft
     *
     * @param sender  Message sender
     * @param message Message
     */
    public void sendToMinecraft(String sender, String message) {
        sendToMinecraft(Lang.MINECRAFT_FORMAT
                .replace("{sender}", sender)
                .replace("{message}", message));
    }

    /**
     * Send message to minecraft
     *
     * @param message Message
     */
    public void sendToMinecraft(String message) {
        // parse any emoticons
        message = EmojiParser.parseToAliases(message);

        // translate any {icon} tags to icon unicode
        if (getPl3xIconsHook() != null) {
            message = getPl3xIconsHook().translate(message);
        }

        Lang.broadcast(Config.PREFIX + message);
    }

    /**
     * Send chat message to discord
     *
     * @param sender  Message sender
     * @param message Message
     */
    public void sendToDiscord(String sender, String message) {
        sendToDiscord(Lang.DISCORD_FORMAT
                .replace("{sender}", sender)
                .replace("{message}", message));
    }

    /**
     * Send message to discord
     *
     * @param message Message
     */
    public void sendToDiscord(String message) {
        if (client == null || !client.isReady()) {
            Logger.error("Could not send message, bot is not connected");
            return;
        }

        if (channel == null) {
            Logger.error("Could not send message, channel is not registered");
            return;
        }

        // untranslate any icon unicode to {icon} tags
        if (getPl3xIconsHook() != null) {
            message = getPl3xIconsHook().untranslate(message);
        }

        // async task needs scope to message
        final String msgToSend = message;

        getServer().getScheduler().runTaskAsynchronously(this, () -> {
            try {
                new MessageBuilder(client)
                        .appendContent(ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', msgToSend)))
                        .withChannel(channel)
                        .build();
            } catch (DiscordException e) {
                Logger.error("Discord threw an exception while sending the message. " + e.getLocalizedMessage().split(" With response text: ")[0]);
            } catch (RateLimitException e) {
                Logger.warn("Bot is currently rate limited. Cannot send message.");
            } catch (MissingPermissionsException e) {
                Logger.error("Your Bot is missing required permission to perform this action! " + e.getLocalizedMessage());
            }
        });
    }
}
