package net.pl3x.bukkit.pl3xbot.event;

import net.pl3x.bukkit.pl3xbot.configuration.Config;
import sx.blah.discord.handle.obj.IMessage;

import java.util.Arrays;

/**
 * Bot command received from discord
 */
public class CommandEvent extends MessageEvent {
    private final IMessage iMessage;
    private final String command;
    private final String[] args;

    public CommandEvent(IMessage iMessage) {
        super(iMessage.getContent(), iMessage.getAuthor().getName());
        this.iMessage = iMessage;

        String[] split = getMessage().split(" ");
        this.command = split[0].substring(Config.COMMAND_TRIGGER.length());
        this.args = Arrays.copyOfRange(split, 1, split.length);
    }

    /**
     * Get the raw discord message that triggered this command
     *
     * @return Discord message
     */
    public IMessage getRawMessage() {
        return iMessage;
    }

    /**
     * Get the command
     *
     * @return Command
     */
    public String getCommand() {
        return command;
    }

    /**
     * Get the command arguments
     *
     * @return Command arguments
     */
    public String[] getArgs() {
        return args;
    }
}
