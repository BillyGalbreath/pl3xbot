package net.pl3x.bukkit.pl3xbot.event;

import org.bukkit.ChatColor;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

import java.util.Arrays;

/**
 * Message received from discord
 */
public class ReceiveMessageEvent extends MessageEvent {
    private final IMessage iMessage;

    public ReceiveMessageEvent(IMessage iMessage) {
        super(iMessage.getContent(), iMessage.getAuthor().getName());
        this.iMessage = iMessage;

        // cleanup mentioned names
        for (IUser u : iMessage.getMentions()) {
            String name = u.getName();
            String id = u.getID();
            setMessage(getMessage().replace("<@" + id + ">", "@" + name));
        }

        // cleanup whitespace and mentioned roles
        String[] part = getMessage().split("\\s+");
        int i = 0;
        for (String tmp : part) {
            if (tmp.startsWith("<@&") && tmp.endsWith(">")) {
                String id = tmp.substring(3, tmp.length() - 1);
                String roleName = iMessage.getGuild().getRoleByID(id).getName();
                part[i] = "@" + roleName;
            }
            i++;
        }

        // put cleaned up message pack together
        setMessage(ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', String.join(" ", Arrays.asList(part)))));
    }

    /**
     * Get the raw discord message
     *
     * @return Discord message
     */
    public IMessage getRawMessage() {
        return iMessage;
    }
}
