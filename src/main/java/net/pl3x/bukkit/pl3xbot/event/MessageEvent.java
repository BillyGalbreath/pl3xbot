package net.pl3x.bukkit.pl3xbot.event;

/**
 * Message to/from discord
 */
public abstract class MessageEvent extends BotEvent {
    private String message;
    private String sender;

    public MessageEvent(String message, String sender) {
        setMessage(message);
        setSender(sender);
    }

    /**
     * Get the message
     *
     * @return Message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set a new message
     *
     * @param message New message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Get the message sender
     * <p>
     * Sender can be null.
     *
     * @return Message sender
     */
    public String getSender() {
        return sender;
    }

    /**
     * Set new message sender
     *
     * @param sender Message sender
     */
    public void setSender(String sender) {
        this.sender = sender;
    }
}
