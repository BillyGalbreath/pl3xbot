package net.pl3x.bukkit.pl3xbot.listener;

import net.pl3x.bukkit.pl3xbot.Pl3xBot;
import net.pl3x.bukkit.pl3xbot.event.SendMessageEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class BukkitListener implements Listener {
    private final Pl3xBot plugin;

    public BukkitListener(Pl3xBot plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        plugin.getServer().getPluginManager().callEvent(new SendMessageEvent(event.getMessage(), event.getPlayer().getName()));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        String msg = event.getJoinMessage();
        if (msg != null && !msg.isEmpty()) {
            plugin.getServer().getPluginManager().callEvent(new SendMessageEvent("*" + msg + "*", null));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        String msg = event.getQuitMessage();
        if (msg != null && !msg.isEmpty()) {
            plugin.getServer().getPluginManager().callEvent(new SendMessageEvent("*" + msg + "*", null));
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerDeath(PlayerDeathEvent event) {
        String msg = event.getDeathMessage();
        if (msg != null && !msg.isEmpty()) {
            plugin.getServer().getPluginManager().callEvent(new SendMessageEvent("*" + msg + "*", null));
        }
    }
}
