package net.pl3x.bukkit.pl3xbot.listener;

import net.pl3x.bukkit.pl3xbot.Pl3xBot;
import net.pl3x.bukkit.pl3xbot.event.ReceiveMessageEvent;
import net.pl3x.bukkit.pl3xbot.event.SendMessageEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class MessageListener implements Listener {
    private final Pl3xBot plugin;

    public MessageListener(Pl3xBot plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMessageReceived(ReceiveMessageEvent event) {
        plugin.getServer().getScheduler().runTaskLater(plugin, () -> handleReceive(event), 1);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMessageSend(SendMessageEvent event) {
        plugin.getServer().getScheduler().runTaskLater(plugin, () -> handleSend(event), 1);
    }

    private void handleReceive(ReceiveMessageEvent event) {
        if (event.isCancelled()) {
            return;
        }

        plugin.sendToMinecraft(event.getSender(), event.getMessage());
    }

    private void handleSend(SendMessageEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (event.getSender() == null) {
            plugin.sendToDiscord(event.getMessage());
        } else {
            plugin.sendToDiscord(event.getSender(), event.getMessage());
        }
    }
}
