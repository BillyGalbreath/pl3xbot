package net.pl3x.bukkit.pl3xbot.listener;

import net.pl3x.bukkit.pl3xbot.Logger;
import net.pl3x.bukkit.pl3xbot.Pl3xBot;
import net.pl3x.bukkit.pl3xbot.configuration.Config;
import net.pl3x.bukkit.pl3xbot.event.CommandEvent;
import net.pl3x.bukkit.pl3xbot.event.ReceiveMessageEvent;
import org.bukkit.Bukkit;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.DisconnectedEvent;
import sx.blah.discord.handle.impl.events.GuildCreateEvent;
import sx.blah.discord.handle.impl.events.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.ReconnectFailureEvent;
import sx.blah.discord.handle.impl.events.ReconnectSuccessEvent;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.Status;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.Image;
import sx.blah.discord.util.RateLimitException;

import java.io.File;

public class DiscordListener {
    private final Pl3xBot plugin;

    public DiscordListener(Pl3xBot plugin) {
        this.plugin = plugin;
    }

    @EventSubscriber
    public void onReady(ReadyEvent event) {
        IDiscordClient client = event.getClient();
        Logger.info(plugin.getName(client) + " connect success.");

        if (Config.STATUS != null && !Config.STATUS.isEmpty()) {
            Status status = Status.game(Config.STATUS.replace("{version}", Bukkit.getBukkitVersion().split("-")[0]));
            client.changeStatus(status);
            Logger.debug("Status set: " + status.toString());
        }

        if (Config.AVATAR != null && !Config.AVATAR.isEmpty()) {
            File avatar = new File(plugin.getDataFolder(), Config.AVATAR);
            if (!avatar.exists()) {
                Logger.warn("Avatar specified in config.yml, but file is not found!");
                return;
            }
            try {
                client.changeAvatar(Image.forFile(avatar));
                Logger.debug("Avatar set: " + avatar.getAbsolutePath());
            } catch (DiscordException | RateLimitException e) {
                Logger.error("Unable to set bot avatar:");
                e.printStackTrace();
            }
        }
    }

    @EventSubscriber
    public void onReconnectSuccess(ReconnectSuccessEvent event) {
        Logger.info(plugin.getName(event.getClient()) + " reconnect success.");
    }

    @EventSubscriber
    public void onReconnectFailure(ReconnectFailureEvent event) {
        Logger.warn(plugin.getName(event.getClient()) + " reconnect failure. (attempt #" + event.getCurAttempt() + ")");
    }

    @EventSubscriber
    public void onDisconnect(DisconnectedEvent event) {
        Logger.warn(plugin.getName(event.getClient()) + " disconnected from discord with reason: " + event.getReason());
    }

    @EventSubscriber
    public void onGuildCreate(GuildCreateEvent event) {
        if (event.getGuild() == null) {
            Logger.warn("Joined null server");
            return;
        }

        event.getClient().getChannels(false).stream()
                .filter(channel -> channel.getName().equals(Config.CHANNEL))
                .forEach(channel -> {
                    plugin.setChannel(channel);
                    Logger.debug("Joined channel #" + channel.getName() + " " + channel.getID());
                });
    }

    @EventSubscriber
    public void onMessageReceived(MessageReceivedEvent event) {
        IMessage message = event.getMessage();
        if (!message.getChannel().equals(plugin.getChannel())) {
            return;
        }

        String content = message.getContent();
        String trigger = Config.COMMAND_TRIGGER;
        if (content.startsWith(trigger) && content.length() > trigger.length()) {
            // call command event
            plugin.getServer().getPluginManager().callEvent(new CommandEvent(message));
            return;
        }

        // call message received event
        plugin.getServer().getPluginManager().callEvent(new ReceiveMessageEvent(message));
    }
}
