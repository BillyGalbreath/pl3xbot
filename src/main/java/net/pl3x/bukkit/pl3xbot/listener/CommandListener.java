package net.pl3x.bukkit.pl3xbot.listener;

import net.pl3x.bukkit.pl3xbot.Pl3xBot;
import net.pl3x.bukkit.pl3xbot.configuration.Lang;
import net.pl3x.bukkit.pl3xbot.event.CommandEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.util.List;
import java.util.stream.Collectors;

public class CommandListener implements Listener {
    private final Pl3xBot plugin;

    public CommandListener(Pl3xBot plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onCommand(CommandEvent event) {
        plugin.getServer().getScheduler().runTaskLater(plugin, () -> handleCommand(event), 1);
    }

    private void handleCommand(CommandEvent event) {
        if (event.isCancelled()) {
            return;
        }

        switch (event.getCommand().toLowerCase()) {
            case "tps":
                plugin.sendToDiscord(Lang.COMMAND_TPS_RESULT.replace("{tps}", String.format("%.2f", plugin.getTPS())));
                break;
            case "list":
                List<String> online = plugin.getServer().getOnlinePlayers()
                        .stream().map(Player::getName)
                        .collect(Collectors.toList());
                plugin.sendToDiscord(Lang.COMMAND_LIST_RESULT
                        .replace("{count}", Integer.toString(online.size()))
                        .replace("{max}", Integer.toString(plugin.getServer().getMaxPlayers()))
                        .replace("{userlist}", String.join(", ", online)));
                break;
            default:
                plugin.sendToDiscord(Lang.UNKNOWN_COMMAND);
        }
    }
}
