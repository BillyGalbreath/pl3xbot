package net.pl3x.bukkit.pl3xbot.command;

import net.pl3x.bukkit.pl3xbot.Logger;
import net.pl3x.bukkit.pl3xbot.Pl3xBot;
import net.pl3x.bukkit.pl3xbot.configuration.Config;
import net.pl3x.bukkit.pl3xbot.configuration.Lang;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import sx.blah.discord.util.DiscordException;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CmdPl3xBot implements TabExecutor {
    private final Pl3xBot plugin;

    public CmdPl3xBot(Pl3xBot plugin) {
        this.plugin = plugin;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 1) {
            return Stream.of("reload", "connect", "disconnect")
                    .filter(name -> name.startsWith(args[0].toLowerCase()))
                    .collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("command.pl3xbot")) {
            Lang.send(sender, Lang.COMMAND_NO_PERMISSION);
            return true;
        }

        if (args.length > 0 && args[0].equalsIgnoreCase("reload")) {
            disconnect(sender);

            Config.reload();
            Lang.reload();

            Lang.send(sender, Lang.RELOAD
                    .replace("{plugin}", plugin.getName())
                    .replace("{version}", plugin.getDescription().getVersion()));

            connect(sender);
            return true;
        }

        if (args.length > 0 && args[0].equalsIgnoreCase("connect")) {
            connect(sender);
            return true;
        }

        if (args.length > 0 && args[0].equalsIgnoreCase("disconnect")) {
            disconnect(sender);
            return true;
        }

        Lang.send(sender, Lang.VERSION
                .replace("{plugin}", plugin.getName())
                .replace("{version}", plugin.getDescription().getVersion()));
        return false; // send usage
    }

    private void connect(CommandSender sender) {
        try {
            plugin.connect();
            Lang.send(sender, Lang.CONNECT_SUCCESS);
        } catch (DiscordException | IllegalStateException e) {
            Logger.error("Connect failed:");
            e.printStackTrace();
            Lang.send(sender, Lang.CONNECT_FAILED);
        }
    }

    private void disconnect(CommandSender sender) {
        try {
            plugin.disconnect();
            Lang.send(sender, Lang.DISCONNECT_SUCCESS);
        } catch (DiscordException | IllegalStateException e) {
            Logger.error("Disconnect failed:");
            e.printStackTrace();
            Lang.send(sender, Lang.DISCONNECT_FAILED);
        }
    }
}
